# Portfolio A


### Overview

---

##### Client

Our client is Scott Logic, a Bristol based software consultancy. They provide advice to other software development companies on topics ranging from the architecture of their system to the agile development process.

##### Domain

At the end of each agile sprint the team has a retrospective meeting. In these meetings a large number of post-it notes are produced, these are used to determine what features will and will not be implemented in the next sprint. Each idea is written on a post-it, these are sorted into 3 different columns; start, stop and continue.

At the end of these retrospective meetings a member of the team is chosen to write up the contents of these notes. This can take a lot of time where this person could be doing more useful work.

##### Project

The proposed solution to this is RetroSnapper, a web-based app where you can upload images of a wall of post-its. The app should be able to read the contents of the notes and reproduce them as text, it may also be able to analyse the semantics of the sentences and sort the notes by their meaning, e.g. if the comment was positive or not.

##### Vision

Our initial vision for the project is for each user to have their own account. These accounts will have different permissions e.g. only the scrum master can upload photos and they should be able to see everything that the application is doing.

To start with the scrum master should have the opportunity to go to a page in which they can upload a photo of a wall of post-its. This and all other photos uploaded should then be stored in a database which communicates with our spring backend. Once the photo has been uploaded it can then be sent to the Google Vision API which should return the handwritten text from each post-it.

There should then be another page containing the output from the vision API, this should allow users to check the output text compared with the input images, as well as allowing them to change the output text if the recognised text is wrong. This text then may be semantically analysed to categorise them, e.g. into positive and negative comments at a very basic level.

Finally, a text document should be produced summarising the notes. This can then be sent between members of the team.

### Requirements

---

With the thinking about the project domain, application range and the key problem, we could affirm that points mentioned below would be the key stakeholders of the entire project system.

    Scott Logic: our client who control, run, profit the system we created.

    User : will use, comment on the system we created.

    Serviceman: will operate and maintain the system.  

    Service company: provide the system.

    Advertisement company: increase the popularity of the system and give an introduction of the app.

    Application store: the platform where connects users and the system.


Actually, we will not focus on all of the stakeholders as we just need to create the web-based app for Scott Logic, so what we are most interested in is the user and Scott Logic(our client), we will have a brief discussion of them.

For the Scott Logic, firstly, they would expect the system could be using normally in the business so that the popularity of the system could increase to profit. Secondly, they expect to get the data of the users so that users who have specified interests could be collected to be sent the advertisements by specified algorithms.  

Users are the most important sector in the whole system, this is because that they are the only one to experiment the system in the real world. Users could be divided as two kinds of people: uploaders and downloaders. We will list three user stories below to have a simplified impression of the role of the user.   

* As the scrum master, when I press the Button I want a window to be displayed allowing me to upload a photo


* As a scrum master, when I press the button I want a window to be displayed allowing me to edit the text transferred automatically.


* As a scrum master, when I press the button I want a window to be displayed allowing me to download the text transferred from the image uploaded.


These three user stories are listed according to the order, so these are the key points of the skeleton of the project. Also we need to consider the exceptional situation when sometimes the function cannot archive the requirements of users, we need to prepare alternative routine for the project. Here are the several spare flows listed below :


If users cannot open the window of allowing them to upload the photo when they press the button, the alternative window would be displayed to remind users that there is something wrong with the app.


If users cannot open the window of allowing them to edit the text transferred automatically, there is another window displayed to remind users that there is something wrong with the editing option or the system, users should wait for a second or refresh the page.


If the photo of the hand-written note cannot be transferred into tests automatically, there is alternative window should be displayed to remind users that the systems cannot distinguish the hand-written notes, users should change photos or give a feedback to the relevant department.


If users cannot download the texts transferred automatically, there should be alternative window displayed to remind users that there is something wrong with downloading, they should check the settings of the system or give a feedback of the problem to the relevant department.


According to the realism, we should consider the integrated process of the system, which means that the sequence of the system is indeed essential, so what we need to consider at the first place is the detailed flow of the system rather than loose functions of the system. Here is the exact flow :

User basic flow:

    Users open the APP and sign in.

    Users take photos of the hand-written notes  

    Users upload the note to the website .

    Users choose the options to transfer the notes into text.

    Users open the web page and log in their account.

    Users could see the image on the web and choose to transfer the image.

    Users check out the transferred text.

    Users choose edit the text if the text is different from the note.

    Users save it after editing.

    Users download the text or send the text to other.

    Users close the web or log out.


##### Domain modelling


According to the structure of the project, we would set a template of the project so that we could have a more clear analysis of the project. The first key point we look into is the relationship of the each element in the system.


##### Entity associations

As we all know, the central point of the system is the servers, we use servers to run on service in the precondition of the fact that servers are attached to the network so that the goals of uploading, downloading could be available. Database owned by database company also plays an important role in the system as we need to obtain the method to transfer the image to texts.


##### Evaluating domain models


The working process should be sequential, otherwise that would be out of order, if users try to something without a precondition, the following steps cannot proceed, which means that domain model could capture each state change. The change of the elements should be available so that we could remove elements and combine multiple elements into one case.


##### Attributes

One key point is the attribute which holds the basic structure of the system. This is because that attributes contain the basic properties of the stakeholders in the system. Here is the attribute listed below:

Uploading: photo taking, button to confirm, window to be displayed
Information setting: information transfer, information edit, button to confirm, window to be displayed.
Downloading: button to confirm, window to be displayed, multiple choices to share.

The analysis of the structure of the project should be comprehensive, in this case, we prefer to consider the stakeholders, user stories, basic flow, functional and non-functional requirements of the project.   



### Architecture

---
Here, We start talking about the requirements and quality attributes that will have an influence on our system architecture:

* The client requires our application to be web-based:
  >The application should be solely designed to be accessed and interacted from a web browser.

* The system should be able to store and retrieve images and their corresponding data:
  >Uploaded images should be stored and retrievable efficiently. The system should also store the processed text for each corresponding image, and it should be retrievable along with the image.

* Images are uploaded to the system:
  >The user of the system should be able to effortlessly upload an image or multiple images from a web environment.

* The system should be able to process uploaded images:
  >The system’s main goal is to process images and convert data within the images into digital text format. Implementing this feature would require us to use a third-party provider. Hence, our system needs to work seamlessly with the external API.

* The user should be able to edit processed image data:
  >User should be able to access previously uploaded images that they’re authorized to access and edit the processed image data.

* The system should be accessible from a mobile device:
  >Since people take pictures mostly on their smartphones, it is paramount that the system be efficiently and effortlessly accessed from a mobile environment. The user should be able use all the features of the system on a mobile environment as it would be able to do on a desktop environment.

* Images uploaded to the system by the user should be visible only to the user:
  >Our system works with user’s images; hence privacy is important. Only the user should be able to see the image. No other party that is connected to the system shall be able to access those images unless authorized by the user to do so.

* Users can delete all their data:
  >Users should be able to permanently delete all their previously uploaded images and data from system at any time from any web browser.

* GDPR compliance:
  >The system is primarily going to be deployed in the UK. Hence, it needs to comply with GDPR. This will dictate our system architecture and development process.



![picture](highLevelArchitecture(1).png)

### Development Testing

A core function that needs to reliably work is to upload the note pictures and store it correctly. Whenever we require to show the pictures or recognize them with vision API, the data retrieved from the database needs to be exactly as same as that we have previously uploaded. Additionally, we also need some unique identifier for each picture when storing them, which enables us to require for a particular picture we need.

To implement relative tests, we need some approach to compare the retrieved data with the original data. To achieve this, an equal method of all possible formats of data needs to be implemented correctly, and original copies of different format of data need to be hardcoded in the test functions in advance, which will be later compared with the retrieved data using the equal method.

Here are some possible test cases we propose.

Test cases|Input|Expected output
---|---|---
Check a single picture is correctly uploaded|A single picture|Match perfectly with the original picture
Check a numbers of pictures are correctly stored|A few of pictures.|All the previous uploaded pictures match with the original data respectively. No loss or overwritten.
Check different formats of pictures are correctly handled.|Different frequently used picture formats that are also supported by vision API, including: JPEG, PNG8, PNG24.|All pictures of different formats match with the original data.
Check the properties of pictures are correctly recorded.|Some pictures with a set of properties, such as dates and names, whose relationship with the related pictures shouldn’t be broken.|When requiring the properties of a picture, the out put should be matched with the original property


### Release Testing

In this testing, one user story is chosen to have detailed discussion about the way of releasing testing.

“As a user, I want to be able to separate my modified hand-written notes online."

The reason that why this story is chosen is because that this is the main structure in the system. The potential users prefer to place the notes in a cloud and transfer the hand-written notes into texts, this is the specified part in the project, which means that this is the main goal of the project, so we need to consider this process at first. Here is the basic flow of the main user story:

1. Upload the photo.
2. Transfer the hand-written notes into texts.
3. Modify the texts transferred if the texts different from the notes.
4. Save the texts or separate to other people.

Test case|To do|Expected output
---|---|---
Check if photo could be uploaded|Take a photo using the app and choose to upload|The same photo displayed on the screen
Check if hand-written notes could be transferred|Choose the option of transferring|Texts should be displayed on the screen
Check if the transferred text could be edited|Choose option to modify the texts|Texts could be changed
Check if the texts could be saved|Save the file|The file could be found in the download files of the computer
Check if the texts could be separated to other people|Separate to other people|Other people could receive the file in their account
