package com.retrosnapper.retrosnapper;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "user_id")
    private Integer UserId;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy="User")
    private Set<Images> images;



    public Integer getId() {
        return UserId;
    }

    public void setId(Integer id) {
        this.UserId = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
