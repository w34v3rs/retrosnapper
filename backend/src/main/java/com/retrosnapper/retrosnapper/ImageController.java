package com.retrosnapper.retrosnapper;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.multipart.support.MultipartFilter;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RestController
public class ImageController {

    private AmazonClient amazonClient;

    private UserRepository userRepository;

    private ImagesRepository imagesRepository;

    @Autowired
    private VisionController visionController;

    @Autowired
    ImageController(AmazonClient amazonClient, UserRepository userRepo, ImagesRepository imageRepo) {
        this.amazonClient = amazonClient;
        this.userRepository = userRepo;
        this.imagesRepository = imageRepo;
    }

    @GetMapping("/requestText")
    public String getImage(@RequestParam(value = "imageName", required = true) String imageName) {
        return "The text for " + imageName;
    }

    @PostMapping(value = "/storeImage", produces = "text/plain;charset = UTF-8")
    public @ResponseBody String storeImage(@RequestPart("file") MultipartFile file) throws Exception {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(baos, true);
        VisionController.detectDocumentText(file, out);
        String text = baos.toString();
//        String path = this.amazonClient.uploadFile(file);
//        Images image = new Images();
//        image.setImgPath(path);
//        image.setImgName(file.getOriginalFilename());
//        image.setImgUserId(userRepository.findById(1).get());
//        image.setImgText(text);
//        imagesRepository.save(image);
        return text;
    }

    @DeleteMapping("/deleteImage")
    public String deleteImage(@RequestPart(value = "url") String fileUrl) {
        imagesRepository.deleteById(fileUrl);
        return this.amazonClient.deleteFileFromS3Bucket(fileUrl);
    }

//    @GetMapping("/extractText")
//    public String extractText(String imageUrl) {
//        String text = visionController.getText(imageUrl);
//
//        return text;
//    }


    @PostMapping("/add")
    public String addNewUser (@RequestParam String name) {
        User n = new User();
        n.setName(name);
        userRepository.save(n);
        return "Saved";
    }

    @GetMapping(path="/all")
    public Iterable<User> getAllUsers() {
        return userRepository.findAll();
    }

}
