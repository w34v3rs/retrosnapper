package com.retrosnapper.retrosnapper;

import org.springframework.data.repository.CrudRepository;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository

public interface ImagesRepository extends CrudRepository<Images, String> {
}
