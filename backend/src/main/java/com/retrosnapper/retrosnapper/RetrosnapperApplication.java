package com.retrosnapper.retrosnapper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RetrosnapperApplication {

    public static void main(String[] args) {
        SpringApplication.run(RetrosnapperApplication.class, args);
    }

    // Bootstrap some test data into the in-memory database
//    @Bean
//    ApplicationRunner init(TodoRepository repository) {
//        return args -> {
//            Stream.of("Buy milk", "Eat", "Write tutorial", "Study Vue.js", "Go kayaking").forEach(name -> {
//                Todo todo = new Todo();
//                todo.setTitle(name);
//                todo.setImage(new File(
//                        getClass().getClassLoader().getResource("Screenshot from 2019-04-12 19-45-35.png").getFile()
//                ));
//                repository.save(todo);
//            });
//            repository.findAll().forEach(System.out::println);
//        };
//    }

//    // Fix the CORS errors
//    @Bean
//    public FilterRegistrationBean simpleCorsFilter() {
//        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
//        CorsConfiguration config = new CorsConfiguration();
//        config.setAllowCredentials(true);
//        // *** URL below needs to match the Vue client URL and port ***
//        config.setAllowedOrigins(Collections.singletonList("http://localhost:8080"));
//        config.setAllowedMethods(Collections.singletonList("*"));
//        config.setAllowedHeaders(Collections.singletonList("*"));
//        source.registerCorsConfiguration("/**", config);
//        FilterRegistrationBean bean = new FilterRegistrationBean<>(new CorsFilter(source));
//        bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
//        return bean;
//    }
}
