package com.retrosnapper.retrosnapper;

import javax.persistence.*;

@Entity
@Table(name = "images")
public class Images {

    @Column(name = "image_name")
    private String ImgName;

    @Id
    @Column(name = "image_path")
    private String ImgPath;

    @ManyToOne
    @JoinColumn(name="user_id", nullable=false)
    private User User;

    @Column(name = "image_text")
    private String text;




    public Images(){}


    public String getImgName() {
        return this.ImgName;
    }

    public void setImgName(String name){
        this.ImgName = name;
    }

    public String getImgPath() {
        return this.ImgPath;
    }

    public void setImgPath(String path){
        this.ImgPath = path;
    }

    public User getImgUserId(){
        return this.User;
    }

    public void setImgUserId(User user){
        this.User = user;
    }

    public String getImgText() {
        return this.text;
    }

    public void setImgText(String text){
        this.text = text;
    }
}
