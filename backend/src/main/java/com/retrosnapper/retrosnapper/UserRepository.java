package com.retrosnapper.retrosnapper;

import org.springframework.data.repository.CrudRepository;

//import com.retrosnapper.retrosnapper.User;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository

public interface UserRepository extends CrudRepository<User, Integer> {
}
